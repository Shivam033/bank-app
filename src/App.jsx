import React from 'react'
import Auth from './components/Auth'
import Banking from './components/Banking'
import Balance from './components/Balance'
import AccountStatus from './components/AccountStatus'

function App() {
  return (
    <div>
        <Auth/>
        <Balance/>
        <Banking/>
        <AccountStatus/>
    </div>
  )
}

export default App