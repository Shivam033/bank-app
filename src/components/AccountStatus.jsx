import React from 'react'
import { useSelector } from 'react-redux'

function AccountStatus() {
    const isSavings = useSelector(state=> state.banking.isSavingsAccount)
  return (
    <div>
        {
        !isSavings ?
            <h1>Savings Account</h1>
            :
            <h1>Current Account</h1>
        }
    </div>
  )
}

export default AccountStatus