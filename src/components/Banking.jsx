import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import {deposit, widthdraw, collectInterest, deleteAccount, toggleAccount} from '../actions/bankingActions'

function Banking() {
    const [amount, setAmount] = useState("");
    const dispatch = useDispatch();

    const handleDeposit=()=>{
        dispatch(deposit(amount));
    }
    const handleWidthdraw =()=>{
        dispatch(widthdraw(amount));
    }
    const handleCollectInterest=()=>{
        dispatch(collectInterest())
    }
    const handleDelete=()=>{
        dispatch(deleteAccount())
    }
    const handleChangeAccount=()=>{
        dispatch(toggleAccount())
    }

  return (
    <div className='="form-group'>
        <input value={amount} onChange={(e)=>setAmount(e.target.value)} type="text" className='form-control'/>
        <button onClick={handleDeposit} className='btn btn-success'>Deposit</button>
        <button onClick={handleWidthdraw} className='btn btn-primary'>Widthdraw</button>
        <button onClick={handleCollectInterest} className="btn btn-warning">Collect Intrest</button>
        <button onClick={handleDelete} className="btn btn-danger">Delete Account</button>
        <button onClick={handleChangeAccount} className="btn btn-secondary">Change Account Type</button>
    </div>
  )
}

export default Banking