// A plain js function
const initialState = {
    balance: 0,
    isSavingsAccount: false,
    isLoggedIn: false
}

export const bankingReducer = (state = initialState, action) => {
    switch (action.type){
        case "DEPOSIT":
            // returning new state
            // new state object 
            // spreading previous state to keep the unchanged values
            return { ...state, balance : state.balance + action.amount};
        case "WIDTHDRAW":
            return { ...state, balance : state.balance - action.amount};
        case "COLLECT_INTEREST":
            return { ...state, balance : (state.balance) *1.03};
        case "DELETE_ACCOUNT":
            return { ...state, balance : 0};
        case "TOGGLE_ACCOUNT":
            return { ...state, isSavingsAccount : !state.isSavingsAccount};
        default:
            // returning old state
            return state
    }
}

// actions
// 1. deposit
const deposit = {
    type: "DEPOSIT",
    amount: 100
}
// 2. widthdraw
const widthdraW = {
    type: "WIDTHDRAW",
    amount: 3
}
// 3. collect interest
const collectInterest = {
    type: "COLLECT_INTEREST",
}
// 4. delete account
const deleteAccount = {
    type: "DELETE_ACCOUNT"
}