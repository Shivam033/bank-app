import { combineReducers } from "redux";

import { bankingReducer } from "./bankingReducer";
import { authReducer } from "./authReducers";

export const rootReducer = combineReducers({
    auth: authReducer,
    banking : bankingReducer
})

// this is how state will look like

// const store = {
//     auth:{
//         isLoggedIn: false
//     }, 
//     banking:{
//         balance: 0,
//         isSavingsAccount: false,
//     },
// };